import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;
import event.Event;
import inputPoint.InputPoint;
import inputPoint.InputPointManager;
import org.springframework.ui.Model;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

/**
 * Created by Sylwia on 5/24/2018.
 */
public class Hello {
    public static void main(String[] args) {
        // write your code here
        System.out.println("Welcome to test");
        // Connect

        InputPointManager mgr = new InputPointManager();
        List<InputPoint> list =  mgr.findAll();
        for(InputPoint inputPoint : list) {
            System.out.println(inputPoint.getId()+" "+ inputPoint.getLocation());
        }


        // Disconnect

    }

}