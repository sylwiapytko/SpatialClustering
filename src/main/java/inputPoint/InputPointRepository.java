package inputPoint;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Sylwia on 5/26/2018.
 */
@Repository
public interface InputPointRepository extends JpaRepository<InputPoint, Long> {
    List<InputPoint> findAll();

}
