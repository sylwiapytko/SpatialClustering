package inputPoint;

import com.vividsolutions.jts.geom.MultiPoint;
import com.vividsolutions.jts.geom.Point;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Sylwia on 5/26/2018.
 */
@Entity
@Table(name="demo_01.fastfoods")
public class InputPoint {
    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    @Column(name = "FF_ID")
    private Long id;

    @Column(name = "GEOM")
    @Type(type = "org.hibernate.spatial.GeometryType")
    private Point location;

    public Long getId() {
        return id;
    }

    private void setId(Long id) {
        this.id = id;
    }



    public Point getLocation() {
        return this.location;
    }

    public void setLocation(Point location) {
        this.location = location;
    }
}
