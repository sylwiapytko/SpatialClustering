package inputPoint;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;
import event.Event;
import event.EventManager;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import util.JPAUtil;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Date;
import java.util.List;

/**
 * Created by Sylwia on 5/26/2018.
 */
@Service
public class InputPointManager {
    @Autowired
    private InputPointRepository inputPointRepository;


    public List<InputPoint> getInputPoints(){
        System.out.println("in");
        List<InputPoint> all = inputPointRepository.findAll();
        return all;
    }
    public List findAll() {
        EntityManager em = JPAUtil.createEntityManager();
        em.getTransaction().begin();
        Query query = em
                .createQuery(
                        "select e from InputPoint e ");
        return query.getResultList();
    }
}
